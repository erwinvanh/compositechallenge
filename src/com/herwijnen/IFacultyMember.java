package com.herwijnen;

import java.util.List;

/**
 * Interface for all other classes
 */
public interface IFacultyMember {
    String getDetails();
    void addFacultyMember(IFacultyMember IFacultyMember);
    void deleteFacultyMember(IFacultyMember IFacultyMember);
    List<IFacultyMember> getIFacultyMemberList();

}
