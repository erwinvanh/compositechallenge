package com.herwijnen;

import java.util.List;

/**
 *
 */
public class Professor implements IFacultyMember {
    private String name;
    private String title;

    /**
     *
     * @param name
     * @param title
     */
    public Professor(String name, String title) {
        this.name = name;
        this.title = title;
    }

    /**
     * Dummy method to satisfy the interface
     * @param IFacultyMember
     */
    @Override
    public void addFacultyMember(IFacultyMember IFacultyMember) {
        throw new UnsupportedOperationException();
    }
    /**
     * Dummy method to satisfy the interface
     * @param IFacultyMember
     */
    @Override
    public void deleteFacultyMember(IFacultyMember IFacultyMember) {
        throw new UnsupportedOperationException();
    }

    /**
     * Dummy method to satisfy the interface
     * @param
     */
    @Override
    public List<IFacultyMember> getIFacultyMemberList() {
        throw new UnsupportedOperationException();
    }

    /**
     * Return details on the Professor
     * @return
     */
    @Override
    public String getDetails() {
        return "\t\t" + name + " is a " + title;
    }
}
