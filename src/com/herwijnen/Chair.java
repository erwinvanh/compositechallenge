package com.herwijnen;

import java.util.ArrayList;
import java.util.List;

/**
 * Chair, contains list of FacultyMembers (professors)
 */
public class Chair implements IFacultyMember {
    private List<IFacultyMember> IFacultyMemberList = new ArrayList<>();
    String name;
    String department;

    public Chair(String name, String department) {
        this.name = name;
        this.department = department;
    }

    @Override
    public void addFacultyMember(IFacultyMember IFacultyMember){
        IFacultyMemberList.add(IFacultyMember);
    }

    @Override
    public void deleteFacultyMember(IFacultyMember IFacultyMember){
        IFacultyMemberList.remove(IFacultyMember);
    }

    @Override
    public List<IFacultyMember> getIFacultyMemberList(){
        return this.IFacultyMemberList;
    }

    @Override
    public String getDetails() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n\t" + name + " is the chair of " + department);
        for (IFacultyMember facultyMember:IFacultyMemberList ){
            sb.append("\n\t\t" + facultyMember.getDetails());
        }
        return sb.toString();

    }
}
