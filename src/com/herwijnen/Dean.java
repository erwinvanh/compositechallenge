package com.herwijnen;

import java.util.ArrayList;
import java.util.List;

/**
 * Highest level, Dean contains one or more departments
 */

public class Dean implements IFacultyMember {
    private List<IFacultyMember> departmentList = new ArrayList<>();
    private String name;
    private String facultyName;

    public Dean(String name, String facultyName) {
        this.name = name;
        this.facultyName = facultyName;
    }

    @Override
    public void addFacultyMember(IFacultyMember IFacultyMember){
        departmentList.add(IFacultyMember);
    }

    @Override
    public void deleteFacultyMember(IFacultyMember IFacultyMember){
        departmentList.remove(IFacultyMember);
    }

    @Override
    public List<IFacultyMember> getIFacultyMemberList() {
        return departmentList;
    }

    @Override
    public String getDetails() {
        StringBuilder sb = new StringBuilder();
        sb.append(name + " is the dean of " + facultyName);
        for (IFacultyMember iFacultyMember:departmentList){
            sb.append(iFacultyMember.getDetails());
        }
        return sb.toString();
    }
}
