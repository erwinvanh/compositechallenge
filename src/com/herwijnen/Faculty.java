package com.herwijnen;

/**
 * Main class, containing a faculty with departments and professors
 */
public class Faculty {

    public static void main(String[] args) {
        // First create all low-level members
        IFacultyMember mathOne = new Professor("Math Professor One", "Adjunct");
        IFacultyMember mathTwo = new Professor("Math Professoe Two", "Associate");
        IFacultyMember cseOne = new Professor("CSE Proffesor One", "Adjunct");
        IFacultyMember cseTwo = new Professor("CSE Proffesor Two", "Professor");
        IFacultyMember cseThree = new Professor("CSE Proffesor Three", "Professor");

        // Now create the Chairs
        IFacultyMember maths = new Chair("Chair Maths", "Maths Department");
        IFacultyMember cse = new Chair("Chair CSE", "Computer Sciences Department");

        // Now creat the Dean
        IFacultyMember dean = new Dean("Dr. S Some","Technology");

        maths.addFacultyMember(mathOne);
        maths.addFacultyMember(mathTwo);
        cse.addFacultyMember(cseOne);
        cse.addFacultyMember(cseTwo);
        cse.addFacultyMember(cseThree);
        dean.addFacultyMember(maths);
        dean.addFacultyMember(cse);

        System.out.println("**** Composite Challenge ***");
        System.out.println(dean.getDetails());
        // Delete a member
        System.out.println("Deleting cseTwo, now the following professors are present");
        System.out.println(dean.getDetails());
    }
}
